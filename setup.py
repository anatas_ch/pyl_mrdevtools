# -*- coding: utf-8 -*-

"""
Setuptool setup for the modul :mod:`mrdevtools`.

.. moduleauthor:: Michael Rippstein <info@anatas.ch>
"""

from setuptools import setup

setup(
    use_scm_version=True,
)
