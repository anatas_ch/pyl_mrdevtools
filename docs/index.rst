.. mrdevtools documentation master file

mrdevtools: hilfsmittel
=======================

.. toctree::
   :maxdepth: 2
   :numbered:
   :caption: Contents:

   styleguide
   mrdevtools
   todo

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
