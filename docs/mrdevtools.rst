============================
``mrdevtools`` - hilfsmittel
============================

.. automodule:: mrdevtools
   :members:
   :undoc-members:
   :private-members:
   :special-members:


``mrdevtools.logtools`` erweiterungen zum modul :mod:`logging`
==============================================================

.. autoclass:: mrdevtools.logtools.LogColorFormatter
   :members:
   :no-undoc-members:
   :private-members:
   :special-members:
