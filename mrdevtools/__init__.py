# -*- coding: utf-8 -*-

"""
Hilfmittel zum entwickeln.

.. moduleauthor:: Michael Rippstein <info@anatas.ch>

"""

# -----------------------------------------------------------------------------
# -- Modul importe
# - standart Module

# - zusätzliche Module

# - eigene Module

# -----------------------------------------------------------------------------
# -- Modul Definitionen

# -- Konstanten

# -- Klassen
# - Fehlerklassen

# - "Arbeitsklassen"

# -- Funktionen

# -----------------------------------------------------------------------------
# -- modul test
if __name__ == '__main__':
    # import doctest
    # doctest.testmod()
    pass
